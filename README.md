# Mustache Java CLI

Mustache Java templating CLI for docker Java containers.
Please refer to https://github.com/spullara/mustache.java for details.

To run:

```
java -jar target/mustache.jar <template/path>
```

For example:

```
export USER=slava
java -jar target/mustache.jar demo.conf
Hello, slava!
```

