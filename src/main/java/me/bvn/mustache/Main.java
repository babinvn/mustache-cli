package me.bvn.mustache;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;

public class Main {
    public static void main(String[] args) throws IOException {
        if (args.length < 1) {
            throw new IllegalArgumentException("Missing template argument");
        }

        MustacheFactory mf = new DefaultMustacheFactory();
        try (
            InputStream in = new FileInputStream(args[0]);
            InputStreamReader r = new InputStreamReader(in);
            Writer writer = new OutputStreamWriter(System.out);
        ) {
            Mustache mustache = mf.compile(r, "template");
            mustache.execute(writer, System.getenv());
        }
    }    
}
